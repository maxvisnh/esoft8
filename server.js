const path = require("path");
const express = require("express");

const app = express();

app.use(express.static(path.resolve(__dirname, "")));
app.set('view engine', 'ejs');
    
let users =[
    {login: "123@gmail.com", password: "3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2"},
    {login: "321@gmail.com", password: "3043aa4a83b0934982956a90828140cb834869135b5f294938865de12e036de440a330e1e8529bec15ddd59f18d1161a97bfec110d2622680f2c714a737d7061"},
    {login: "strong@gmail.com", password: "5ec2a9694c970a096879967ab26ddb8ea5e85da4a5c93ee02e1fac60782d63e04e75c72669454bd5923e66adaa98f6444fdbf14a71b39306f9abc0c42bce7971"}
];

app.get('/', function(req, res) {
    res.render('pages/auth');
});

app.post("/", function(req, res){
    let foundUser;
    let hashPass = req.body.password;
    const password = crypto.createHash("sha512").update(hashPass).digest("hex");

    for (let i = 0; i< users.length; i++){
        let u = users[i];
        if(req.body.login == u.login && password == u.password){
            foundUser = u.login;
            break;
        }
    }
    if (foundUser !== undefined){
        res.send("Login successful!")
    }
    else{
        res.status(401).send("login error!");
    }
});
app.get('/raiting', function(req, res) {
    let leaderBoard = [
        { name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%"},
        { name: 'Александров Игнат ', gameCount: "324235", win: 2012, lose:423, winRate:"24%"},
        { name: 'Александров ', gameCount: "53254", win: 2012, lose:423, winRate:"24%"},
        { name: 'Игнат Анатолиевич', gameCount: "54325", win: 2012, lose:423, winRate:"24%"},
        { name: 'Александров Игнат ', gameCount: "43643", win: 2012, lose:423, winRate:"24%"}
    ];
    res.render('pages/raiting', {leaderBoard: leaderBoard});
});

app.listen(8080);
console.log('8080 is the magic port');

