const crypto = require("crypto");

const hash=crypto.createHash("sha512").update("123").digest("hex");
const hash2=crypto.createHash("sha512").update("123").digest("hex");
const hash3=crypto.createHash("sha512").update("321").digest("hex");
const hash4=crypto.createHash("sha512").update("StrongPassword123").digest("hex");

console.log(hash + "\n", hash2 + "\n", hash3 + "\n", hash4);